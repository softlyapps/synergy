package com.softly.synergy;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.LinearLayout;

// Custom Stepper, based on: https://github.com/czy1121/numberstepper
public class View_Stepper extends LinearLayout
{
    private int mStep = 1, mValue = 0, mMaxValue = 0, mMinValue = 0;
    View_StepperButton btnMinus, btnPlus;
    TextView txtValue;
    
    OnValueChangedListener mOnValueChanged;

    public View_Stepper(Context context) {
        this(context, null);
    }

    public View_Stepper(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public View_Stepper(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public View_Stepper(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(int step, int min, int max, int value)
    {
        mStep = Math.max(step, 1);
        mValue = Integer.MAX_VALUE;
        
        if (min == max)
        {
            mMinValue = max;
            mMaxValue = max;
        }
        else
        {
            mMinValue = Math.min(min, max);
            mMaxValue = Math.max(min, max);
        }
        
        if (mStep != 1)
        {
            mMinValue = normalize(mMinValue);
            mMaxValue = normalize(mMaxValue);
        }
        
        setValue(value, false);
    }

    private void init(Context context, AttributeSet attrs)
    {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NumberStepper);

        try
        {
            setOrientation(HORIZONTAL);
			setFocusableInTouchMode(true);
            
            btnMinus = new View_StepperButton(context, null, R.attr.styleLeft);
			txtValue = new TextView(context, null, R.attr.styleValue);
			btnPlus = new View_StepperButton(context, null, R.attr.styleRight);
            
            mStep = a.getInt(R.styleable.NumberStepper_step, 1);
            mValue = a.getInt(R.styleable.NumberStepper_value, 0);
            mMinValue = a.getInt(R.styleable.NumberStepper_minValue, Integer.MIN_VALUE);
            mMaxValue = a.getInt(R.styleable.NumberStepper_maxValue, Integer.MAX_VALUE);
            
            init(mStep, mMinValue, mMaxValue, mValue);
            
            addView(btnMinus, new LayoutParams(0, LayoutParams.MATCH_PARENT));
			addView(txtValue, new LayoutParams(0, LayoutParams.MATCH_PARENT, 1));
			addView(btnPlus, new LayoutParams(0, LayoutParams.MATCH_PARENT));
			
			setListeners();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            a.recycle();
        }
    }
    
    public void setOnValueChangedListener(OnValueChangedListener onValueChanged)
    {
        mOnValueChanged = onValueChanged;
    }

    public void notifyValueChanged()
    {
        if (mOnValueChanged != null)
        {
            mOnValueChanged.onValueChanged(this, mValue);
        }
    }
    
    public int getValue()
    {
        return mValue;
    }

    public void setValue(int value)
    {
        setValue(value, true);
    }

    public void setValue(int value, boolean notifyValueChanged)
    {
        if (value == mValue)
        {
            return;
        }
        
        int valid = Math.min(Math.max(normalize(value), mMinValue), mMaxValue);
        txtValue.setText(String.valueOf(valid));
        
        if (valid == mValue)
        { // valid != value
            return;
        }
        
        mValue = valid;
        btnMinus.setEnabled(mValue != mMinValue);
        btnPlus.setEnabled(mValue != mMaxValue);
        
        if (notifyValueChanged && mOnValueChanged != null)
        {
            mOnValueChanged.onValueChanged(this, mValue);
        }
    }

    private int normalize(int value)
    {
        return value - value % mStep;
    }

    private void setListeners()
    {
        btnMinus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int newValue = mValue - mStep;
				if(newValue >= mMinValue)
				{
					setValue(newValue, true);
				}
            }
        });
        
        btnPlus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int newValue = mValue + mStep;
				if(newValue <= mMaxValue)
				{
					setValue(newValue, true);
				}
            }
        });
    }

    public interface OnValueChangedListener
    {
        void onValueChanged(View_Stepper view, int value);
    }
}


