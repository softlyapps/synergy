package com.softly.synergy;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class View_StepperButton extends Button
{
	private long initialWait = 500;
	private long repeatIntervalMs = 100;
	
	private Runnable repeatClickRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			performClick();

			if(isEnabled())
			{
                postDelayed(repeatClickRunnable, repeatIntervalMs);
			}
		}
	};
	
	public View_StepperButton(Context context)
	{
        this(context, null);
        init();
    }

    public View_StepperButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public View_StepperButton(Context context, AttributeSet attrs, int defStyleAttr)
    {
        this(context, attrs, defStyleAttr, 0);
        init();
    }

    public View_StepperButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr);
        init();
    }
    
    private void init()
	{
		this.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				int action = event.getAction();
				
                if(action == MotionEvent.ACTION_DOWN) 
                {
                    removeCallbacks(repeatClickRunnable);
                    performClick();
                    
                    if(v.isEnabled())
                    {
                        postDelayed(repeatClickRunnable, initialWait);
                    }
                }
                else if(action == MotionEvent.ACTION_UP)
                {
                    // Cancel any repetition in progress.
                    removeCallbacks(repeatClickRunnable);
                }

                return true;
			}
		});
	}
    
    private int measureDimension(int desiredSize, int measureSpec)
    {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY)
        {
            result = specSize;
        }
        else
        {
            result = desiredSize;
            if (specMode == MeasureSpec.AT_MOST)
            {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }
    
    @Override protected void
    onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
		int desiredWidth = getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight();
        int desiredHeight = getSuggestedMinimumHeight() + getPaddingTop() + getPaddingBottom();

		int measuredWidth = measureDimension(desiredWidth, widthMeasureSpec);
		int measuredHeight = measureDimension(desiredHeight, heightMeasureSpec);

		if(measuredWidth > measuredHeight) // Maintain 1:1 aspect ratio
		{
			measuredHeight = measuredWidth;
		}
		else if(measuredWidth < measuredHeight)
		{
			measuredWidth = measuredHeight;
		}

        setMeasuredDimension(measuredWidth, measuredHeight);
	}
}


