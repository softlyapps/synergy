package com.softly.synergy;

public class Object_Player extends Object_Entry
{
	int correctExplanaitions;
	int correctInterpretations;
	
	public Object_Player(String name)
	{
		super(name);
        
		this.correctExplanaitions = 0;
		this.correctInterpretations = 0;
	}
}
