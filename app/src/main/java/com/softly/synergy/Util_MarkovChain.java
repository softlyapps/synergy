package com.softly.synergy;

import java.io.InputStream;
import java.util.Random; // <- Do we need this?

public class Util_MarkovChain
{
    private static final int CHAR_SET_SIZE = 26;
    private static final int WEIGHT_SET_SIZE = 16;
    
    private long chainDepth;
    private byte[] weights; // Will be read as nibbles
    public boolean success = true;
    
    public Util_MarkovChain(InputStream rawData)
    {
        try
        {
            byte[] depthBuffer = new byte[4];
            rawData.read(depthBuffer, 0, 4);
            
            chainDepth = depthFromBuffer(depthBuffer);
            
            if(chainDepth > 3) // We don't support depth over 3, let's keep it small
            {
                throw new Exception("Depth of Markov Chain is not supported");
            }
            
            int weightsByteSize = byteSizeFromDepth(chainDepth);
            weights = new byte[weightsByteSize];
            
            rawData.read(weights, 0, weightsByteSize);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    private long depthFromBuffer(byte[] buffer)
    {
        long depth = 0;
        depth += (buffer[0] & 0xFF);
        depth += (buffer[1] & 0xFF) << 8;
        depth += (buffer[2] & 0xFF) << 16;
        depth += (buffer[3] & 0xFF) << 24;
        
        return depth;
    }
    
    private int byteSizeFromDepth(long depth)
    {
        int weightSetAmount = 0;
            
        for(int i = 0; i <= depth; ++i)
        {
            weightSetAmount += Math.pow(CHAR_SET_SIZE, i);
        }
        
        return (weightSetAmount * WEIGHT_SET_SIZE);
    }
    
    private int indexFromChar(char c)
    {
        if(c > 64 && c < 91) // upper-case
        {
            return (c - 65);
        }
        else if(c > 96 && c < 123) // lower-case
        {
            return (c - 97);
        }
        
        return CHAR_SET_SIZE; // Not a good solution
    }
    
    private char charFromIndex(int index)
    {
        return (char)(index + 97);
    }
    
    private int sumFromWeightSet(int weightSetIndex)
    {
        int sumIndex = weightSetIndex + 13;
        int sum = 0;
        
        sum += (weights[sumIndex++] & 0xFF) << 8;
        sum += (weights[sumIndex++] & 0xFF);
        
        return sum;
    }
    
    private char charFromWeightSet(int weightSetIndex, int weight)
    {
        int curWeight = 0;
        int setIndex = weightSetIndex;
        
        for(int letterIndex = 0; letterIndex < CHAR_SET_SIZE; ++letterIndex)
        {
            if((letterIndex & 0x01) == 0x01) // Odd
            {
                curWeight += (weights[setIndex] & 0x0F);
                setIndex++;
            }
            else // Even
            {
                curWeight += (weights[setIndex] & 0xF0) >> 4;
            }
            
            if(curWeight > weight)
            {
                return charFromIndex(letterIndex);
            }
        }
        
        return '\0';
    }
    
    private int weightSetIndexInChain(String name, int nextLetterIndex)
    {
        long depth = (nextLetterIndex < chainDepth) ? nextLetterIndex : chainDepth;
        int weightIndex = 0;
        
        for(int d = 0; d < depth; ++d) // Skip wrong depth-levels
        {
            weightIndex += Math.pow(CHAR_SET_SIZE, d) * WEIGHT_SET_SIZE;
        }
        
        for(int d = 0; d < depth; ++d) // Go to index within correct depth-level
        {
            char c = name.charAt(nextLetterIndex - (d + 1));
            int letterIndex = indexFromChar(c);
            
            weightIndex += ((int)Math.pow(CHAR_SET_SIZE, d) * WEIGHT_SET_SIZE) * letterIndex;
        }
        
        return weightIndex;
    }
    
    public String generateName(int maxLength)
    {
        String name = new String();
        
        for(int c = 0; c < maxLength; ++c)
        {
            int weightSetIndex = weightSetIndexInChain(name, c);
            int sum = sumFromWeightSet(weightSetIndex);
            
            if(sum > 0)
            {
                // What happens when weight always equals sum?
                int weight = (int)Math.round(Math.random() * (sum - 1));
                name += charFromWeightSet(weightSetIndex, weight);
                
                if(c == 0) { name = name.toUpperCase(); } // Capitalize first letter
            }
            else // No valid continuation
            {
                c = maxLength;
            }
        }
        
        return name;
    }
    
    public String generateName()
    {
        int maxLength = (int)Math.round(Math.random() * 4) + 4;
        return generateName(maxLength);
    }
}
