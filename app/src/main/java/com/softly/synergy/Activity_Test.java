package com.softly.synergy;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.graphics.Color;
import android.os.Handler;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.BaseAdapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ToggleButton;
import android.widget.TextView;
import android.widget.EditText;
import java.util.Date;
import android.content.Intent;
import android.util.Log;

import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import android.util.AttributeSet;

import android.widget.ArrayAdapter;
import android.content.Context;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import android.view.LayoutInflater;
import android.util.DisplayMetrics;
import android.text.TextWatcher;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;

import java.io.Serializable;

public class Activity_Test extends Activity_Base
{	
    private final String TAG = "synergy";

    Context context;
    LayoutInflater inflater;

    Spinner testDropdown;
    EditText playerName;
    Button addPlayer;
    Button removePlayer;
    Button btnTestOne;
    Button btnTestTwo;
    
    GridView playerGrid;
    
    ArrayList<Object_Player> playerArray;
    Adapter_Player playerAdapter;

    Util_MarkovChain markovChain;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        
        context = getApplicationContext();
        inflater = getLayoutInflater();
        
        try
        {
            testDropdown = findViewById(R.id.test_dropdown);
            playerName = findViewById(R.id.test_playerName);
            addPlayer = findViewById(R.id.test_btnPlayerAdd);
            removePlayer = findViewById(R.id.test_btnPlayerRemove);
            btnTestOne = findViewById(R.id.test_btnTestOne);
            btnTestTwo = findViewById(R.id.test_btnTestTwo);
            playerGrid = findViewById(R.id.test_playerGrid);
            
            playerArray = new ArrayList<Object_Player>();
            playerAdapter = new Adapter_Player(this, playerArray);
            playerGrid.setAdapter(playerAdapter);
            
            InputStream in = getResources().openRawResource(R.raw.markov_weights);
            markovChain = new Util_MarkovChain(in);
            
            addListeners();
        }
        catch(Exception e)
        {
            Log.d(TAG, e.getMessage());
        }
    }
    
    public void addPlayer()
    {
        try
        {
            String name = playerName.getText().toString();
            if(name.isEmpty())
            {
                name = markovChain.generateName();
            }
            
            boolean unique = true;
            for(int i = 0; unique && i < playerGrid.getChildCount(); ++i)
            {
                ToggleButton curBtn = (ToggleButton)playerGrid.getChildAt(i);
                unique = !name.equals(curBtn.getText());
            }
            
            if(unique)
            {
                Log.d(TAG, "Adding " + name);
                
                //gAppState.createPlayer(name);
                
                playerArray.add(new Object_Player(name));
                playerAdapter.notifyDataSetChanged();
                playerGrid.smoothScrollToPosition(playerArray.size());
            }
            else
            {
                Toast.makeText(context, "That player already exists!", Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception e)
        {
            Log.d(TAG, "Something went wrong...");
            Log.d(TAG, e.getMessage());
        }
    }
    
    public void addListeners()
    {
        addPlayer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addPlayer();
            }
        });
        
        playerName.setOnEditorActionListener(new EditText.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if(actionId == EditorInfo.IME_ACTION_DONE)
                {
                    addPlayer();
                    return true;
                }
                
                return false;
            }
        });
        
        removePlayer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                
            }
        });
        
        btnTestOne.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                
            }
        });
        
        btnTestTwo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
            
            }
        });
        
        playerGrid.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int pos, long id)
            {
                Log.d(TAG, "Clicked grid");
            }
        });
    }
}
