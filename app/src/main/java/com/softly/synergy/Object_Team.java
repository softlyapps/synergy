package com.softly.synergy;

import java.util.ArrayList;

public class Object_Team extends Object_Entry
{
	ArrayList<Object_Player> members;
	
	int score;
	boolean temporary; // Should this team be removed after the game is over?
	
	public Object_Team(String name, boolean temporary)
	{
		super(name);
		
		this.members = new ArrayList<Object_Player>();
		this.temporary = temporary;
		this.score = 0;
	}
	
	public Object_Team(String name) { this(name, false); }
	
	ArrayList<Object_Player> getMembers()
	{
        return members;
	}
}
