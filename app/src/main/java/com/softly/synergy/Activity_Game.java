package com.softly.synergy;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.view.ViewGroup.LayoutParams;
import android.graphics.Color;
import android.os.Handler;
import android.widget.Button;
import android.widget.ToggleButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.Date;
import android.content.Intent;
import android.util.Log;

public class Activity_Game extends Activity_Base
{
    private static final String TAG = "thirty_seconds";

    ProgressBar progressBar;
    TextView progressText;

    ToggleButton btnG1;
    ToggleButton btnG2;
    ToggleButton btnG3;
    ToggleButton btnG4;
    ToggleButton btnG5;

	private int timeElapsedMs = 0;
    private int msSteps = 10;
    private long timeStampMs;

    private Handler progressBarHandler = new Handler();
    
    private Handler progressTimerHandler = new Handler();
    private Runnable progressTimerRunnable = new Runnable()
    {
		@Override
		public void run()
		{
			if(timeElapsedMs < gAppState.gameConf.roundTimeMs)
			{
				timeElapsedMs = (int)(System.currentTimeMillis() - timeStampMs);

				progressBarHandler.post(new Runnable()
				{
					public void run()
					{
						progressText.setText(String.valueOf((int)Math.ceil((gAppState.gameConf.roundTimeMs - timeElapsedMs) / 1000.0)));
						progressBar.setProgress(gAppState.gameConf.roundTimeMs - timeElapsedMs);
					}
				});
				
				progressTimerHandler.postDelayed(this, msSteps);
			}
			else
			{
				stopGame();
			}
		}
	};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(TAG, "OnCreate GameActivity");

        try
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_game);

            progressBar = findViewById(R.id.prgBar);
            progressText = findViewById(R.id.prgText);
            btnG1 = findViewById(R.id.btnG1);
            btnG2 = findViewById(R.id.btnG2);
            btnG3 = findViewById(R.id.btnG3);
            btnG4 = findViewById(R.id.btnG4);
            btnG5 = findViewById(R.id.btnG5);

			addButtonListeners();
            startGame();
        }
        catch(Exception e)
        {
            Log.d(TAG, "Something went wrong...");
            Log.d(TAG, e.getMessage());
        }
    }

	public void startGame()
	{
		timeStampMs = System.currentTimeMillis();
        timeElapsedMs = 0;

        progressBar.setMax(gAppState.gameConf.roundTimeMs);
        progressBar.setProgress(gAppState.gameConf.roundTimeMs);

        progressText.setText(String.valueOf((int)Math.floor(gAppState.gameConf.roundTimeMs / 1000)));
        progressTimerHandler.postDelayed(progressTimerRunnable, 0);
	}

    public void stopGame()
    {
        progressTimerHandler.removeCallbacks(progressTimerRunnable);

        if(btnG1.isChecked()) { gAppState.gameState.elementsCorrect++; }
        if(btnG2.isChecked()) { gAppState.gameState.elementsCorrect++; }
        if(btnG3.isChecked()) { gAppState.gameState.elementsCorrect++; }
        if(btnG4.isChecked()) { gAppState.gameState.elementsCorrect++; }
        if(btnG5.isChecked()) { gAppState.gameState.elementsCorrect++; }

        finish();
    }
    
    public void addButtonListeners() // Could be a for-loop when amount of buttons on screen is dynamic
    {
        btnG1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(reachedLimit())
                {
					stopGame();
				}
            }
        });
        
        btnG2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(reachedLimit())
                {
					stopGame();
				}
            }
        });
        
        btnG3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(reachedLimit())
                {
					stopGame();
				}
            }
        });
        
        btnG4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(reachedLimit())
                {
					stopGame();
				}
            }
        });
        
        btnG5.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(reachedLimit())
                {
					stopGame();
				}
            }
        });
    }
    
    public boolean reachedLimit()
    {
		return (btnG1.isChecked() && btnG2.isChecked() && btnG3.isChecked() && btnG4.isChecked() && btnG5.isChecked());
	}
}
