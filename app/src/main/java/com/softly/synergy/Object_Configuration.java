package com.softly.synergy;

public class Object_Configuration implements java.io.Serializable
{
	public int gameMode = 0;
	public int allianceType = 0;
	public int scoreDisplay = 0;

	public boolean replayEnabled = false;
	public boolean skipEnabled = true;
	public int skipPenalty = 30;

	public int roundTimeMs = 30000;
	public int roundLimit = 2;
	public int itemsPerRound = 5;
}
