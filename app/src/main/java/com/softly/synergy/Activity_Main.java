package com.softly.synergy;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.graphics.Color;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;
import java.util.Date;
import android.content.Intent;
import android.util.Log;

public class Activity_Main extends Activity_Base
{
    private final String TAG = "synergy";

    Button btnStart;
    Button btnConfig;
    Button btnTest;
    TextView tvResult;

    int test;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            gLoadAppState(getApplicationContext());
            
            btnStart = findViewById(R.id.btnStart);
            btnConfig = findViewById(R.id.btnConfig);
            btnTest = findViewById(R.id.btnTest);
            tvResult = findViewById(R.id.txtResult);

            addListenerOnButtonClick();
        }
        catch(Exception e)
        {
            Log.d(TAG, "Something went wrong...");
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    protected void onResume()
    {
        try
        {
            super.onResume();
            tvResult.setText(String.valueOf(gAppState.gameState.elementsCorrect));
        }
        catch(Exception e)
        {
            Log.d(TAG, "Something went wrong...");
            Log.d(TAG, e.getMessage());
        }
    }

    public void addListenerOnButtonClick()
    {
        btnStart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    gAppState.gameState.curRound = 0;
                    
                    Intent intermission = new Intent(Activity_Main.this, Activity_Intermission.class);
                    startActivity(intermission);
                }
                catch(Exception e)
                {
                    Log.d(TAG, "Something went wrong...");
                    Log.d(TAG, e.getMessage());
                }
            }
        });

        btnConfig.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    Intent config = new Intent(Activity_Main.this, Activity_Configuration.class);
                    startActivity(config);
                }
                catch(Exception e)
                {
                    Log.d(TAG, "Something went wrong...");
                    Log.d(TAG, e.getMessage());
                }
            }
        });
        
        btnTest.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    Intent test = new Intent(Activity_Main.this, Activity_Test.class);
                    startActivity(test);
                }
                catch(Exception e)
                {
                    Log.d(TAG, "Something went wrong...");
                    Log.d(TAG, e.getMessage());
                }
            }
        });
    }
}
