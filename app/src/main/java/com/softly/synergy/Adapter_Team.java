package com.softly.synergy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import java.util.ArrayList;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;

public class Adapter_Team extends ArrayAdapter<Object_Team>
{
    public Adapter_Team(Context context, ArrayList<Object_Team> arrayList)
    {
        super(context, 0, arrayList);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent)
    {
        ToggleButton currentItemView = (ToggleButton)convertView;
        AdapterView adapterParent = (AdapterView)parent;
        
        if (currentItemView == null)
        {
            currentItemView = (ToggleButton)LayoutInflater.from(getContext()).inflate(R.layout.entry_team, parent, false);
        }
        
        Object_Entry entry = getItem(pos);
        long id = getItemId(pos);
        
        currentItemView.setText(entry.name);
        currentItemView.setTextOn(entry.name);
        currentItemView.setTextOff(entry.name);
        currentItemView.setChecked(false);
  
        currentItemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                adapterParent.performItemClick(v, pos, id);
            }
        });
        
        return (View)currentItemView;
    }
}
