package com.softly.synergy;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;

import android.net.Uri;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import java.util.ArrayList;
import java.util.List;
import java.io.InputStream;

import android.util.Log;

public class Activity_Configuration extends Activity_Base implements OnItemSelectedListener
{
    private static final String TAG = "synergy";

    Button btnPlayersAndTeams;
    Button btnCategories;

    Spinner scoringSystemSelect;
    Spinner allianceTypeSelect;
    Spinner scoreDisplaySelect;
    
    View_Stepper stpRoundLimit;
    View_Stepper stpItemsOnScreen;
    View_Stepper stpSecondsPerTurn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_configuration);

            btnPlayersAndTeams = findViewById(R.id.btnPlayersAndTeams);
            btnCategories = findViewById(R.id.btnCategories);

            scoringSystemSelect = findViewById(R.id.spnGameMode);
            allianceTypeSelect = findViewById(R.id.spnAllianceType);
            scoreDisplaySelect = findViewById(R.id.spnScoreDisplay);
            
            stpRoundLimit = findViewById(R.id.stpRounds);
            stpItemsOnScreen = findViewById(R.id.stpItems);
            stpSecondsPerTurn = findViewById(R.id.stpSeconds);
            
            setListeners();
        }
        catch(Exception e)
        {
            Log.d(TAG, "Something went wrong...");
            Log.d(TAG, e.getMessage());
        }
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        setCurrentValues();
    }
    
    @Override
    protected void onPause()
    {
        super.onStop();
        gSaveAppState(getApplicationContext());
    }

    private void setListeners()
    {
        scoringSystemSelect.setOnItemSelectedListener(this);
        allianceTypeSelect.setOnItemSelectedListener(this);
        scoreDisplaySelect.setOnItemSelectedListener(this);
        
        stpRoundLimit.setOnValueChangedListener(new View_Stepper.OnValueChangedListener()
        {
            @Override
            public void onValueChanged(View_Stepper stepper, int value)
            {
                gAppState.gameConf.roundLimit = value;
            }
        });
        
        stpItemsOnScreen.setOnValueChangedListener(new View_Stepper.OnValueChangedListener()
        {
            @Override
            public void onValueChanged(View_Stepper stepper, int value)
            {
                gAppState.gameConf.itemsPerRound = value;
            }
        });
        
        stpSecondsPerTurn.setOnValueChangedListener(new View_Stepper.OnValueChangedListener()
        {
            @Override
            public void onValueChanged(View_Stepper stepper, int value)
            {
                gAppState.gameConf.roundTimeMs = value * 1000;
            }
        });
        
        btnPlayersAndTeams.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent playersAndTeams = new Intent(Activity_Configuration.this, Activity_PlayersAndTeams.class);
                startActivity(playersAndTeams);
            }
        });
        
        btnCategories.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent fileDialog = new Intent().setType("*/*").setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(fileDialog, "Select a .pck file"), 1);
            }
        });
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        
        if(requestCode == 1 && resultCode == RESULT_OK)
        {
            Uri uri = data.getData();
            String path = uri.getPath();
            
            if(path.endsWith(".txt"))
            {
                try
                {
                    InputStream input = getContentResolver().openInputStream(uri);
                    Object_Pack pack = new Object_Pack(input);
                }
                catch(Exception e)
                {
                    Log.d("synergy", e.getMessage());
                }
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if(parent.getId() == scoringSystemSelect.getId())
        {
            gAppState.gameConf.gameMode = position;
        }
        else if(parent.getId() == allianceTypeSelect.getId())
        {
            gAppState.gameConf.allianceType = position;
        }
        else if(parent.getId() == scoreDisplaySelect.getId())
        {
            gAppState.gameConf.scoreDisplay = position;
        }
    }

    public void onNothingSelected(AdapterView<?> arg0) { } // Auto-generated method stub
    
    private void setCurrentValues()
    {
        scoringSystemSelect.setSelection(gAppState.gameConf.gameMode);
        allianceTypeSelect.setSelection(gAppState.gameConf.allianceType);
        scoreDisplaySelect.setSelection(gAppState.gameConf.scoreDisplay);
        
        stpRoundLimit.setValue(gAppState.gameConf.roundLimit);
        stpItemsOnScreen.setValue(gAppState.gameConf.itemsPerRound);
        stpSecondsPerTurn.setValue(gAppState.gameConf.roundTimeMs / 1000);
        
        // Add the others!
    }
}
