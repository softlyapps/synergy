package com.softly.synergy;

import android.app.Activity;
import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileNotFoundException;

public class Activity_Base extends Activity
{
    private final String saveFile = "AppState.bin";
    public static Object_AppState gAppState; // Load this in Activity_Main.onCreate()
    
    public void gSaveAppState(Context appContext)
    {
        if(gAppState != null)
        {
            appContext.deleteFile(saveFile); // necessary?
            
            try
            {
                FileOutputStream fos = appContext.openFileOutput(saveFile, Context.MODE_PRIVATE);
                ObjectOutputStream out = new ObjectOutputStream(fos);
                
                out.writeObject(this.gAppState);
                out.close();
                fos.close();
            }
            catch(Exception e)
            {
                // Couldn't save, what to do?
            }
        }
    }
    
    public void gLoadAppState(Context appContext)
    {
        try
        {
            FileInputStream fis = appContext.openFileInput(saveFile);
            ObjectInputStream in = new ObjectInputStream(fis);
            
            this.gAppState = (Object_AppState)in.readObject();
            in.close();
            fis.close();
        }
        catch(FileNotFoundException e) // If file doesn't exist, construct default Object_AppState.
        {
            this.gAppState = new Object_AppState();
        }
        catch(Exception e)
        {
            // Now what?
        }
    }
}
