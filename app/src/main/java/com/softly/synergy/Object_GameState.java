package com.softly.synergy;

public class Object_GameState implements java.io.Serializable
{
	public int[] teams; // Contains ids of teams, is filled before start of game
	public int[] explainers; // Contains ids of explainers, is filled before every round
	public int[] interpreters; // Contains ids of interpreters, is filled before every round

	public int curRound = 0;
	public int curTurn = 0;
	public int turnAmount = 0; // Is calculated before start of game, based on amount of players and gamemode

	public int elementsCorrect = 0;
	public int elementsCorrectRecord = 0;
}
