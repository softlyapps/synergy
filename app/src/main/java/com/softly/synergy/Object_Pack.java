package com.softly.synergy;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import java.nio.charset.StandardCharsets;

import android.util.Log;

public class Object_Pack
{
	String author;
	String language;
	
	public Object_Pack(InputStream input)
	{
        try
        {
            int curIndex = 0;
            
            while(input.available() > 0)
            {
                byte[] b = new byte[1];
                input.read(b, 0, 1);
                String content = new String(b, StandardCharsets.UTF_8);
                Log.d("synergy", content);
            }
            
            
            /*
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            
            while(!reader.ready());
            
            String line = reader.readLine();
            while(line != null)
            {
                Log.d("synergy", line);
                
                line = reader.readLine();
            }*/
        }
        catch(Exception e)
        {
            Log.d("synergy", e.getMessage());
        }
	}
}
