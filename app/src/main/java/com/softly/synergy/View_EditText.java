package com.softly.synergy;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class View_EditText extends EditText
{
    public View_EditText(Context context)
	{
        super(context, null);
    }

    public View_EditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public View_EditText(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr, 0);
    }
    
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event)
    {
        if(event.getKeyCode() == KeyEvent.KEYCODE_BACK)
        {
            dispatchKeyEvent(event);
            clearFocus();
            
            return false;
        }
        
        return super.onKeyPreIme(keyCode, event);
    }
}


