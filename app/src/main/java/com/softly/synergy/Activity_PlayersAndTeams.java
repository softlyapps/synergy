package com.softly.synergy;

import android.content.Context;
import android.os.Bundle;
import java.io.InputStream;

import android.view.View;
import android.view.LayoutInflater;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.EditorInfo;
import android.view.KeyEvent;
import android.view.MotionEvent;

import android.text.Editable;
import android.text.TextWatcher;
import android.graphics.Rect;

import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.ToggleButton;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.AdapterView;
import android.widget.Toast;

import android.util.Log;

public class Activity_PlayersAndTeams extends Activity_Base
{
    private final String TAG = "synergy";
    
    Context context;
    LayoutInflater inflater;
    InputMethodManager imm;
    
    Util_MarkovChain markovChain;
    
    Button btnGenPlayerName;
    Button btnAddPlayer;
    Button btnDeletePlayers;
    Button btnClearPlayerSelection;
    Button btnAddToTeam;
    
    Button btnGenTeamName;
    Button btnAddTeam;
    Button btnRemoveFromTeam;
    Button btnDeleteTeam;
    Button btnShowTeamList;
    Button btnClearTeamSelection;
    
    TextView txtTeamDescription;
    
    View_EditText txtPlayerName;
    View_EditText txtTeamName;
    
    LinearLayout viewTeamOptions;
    LinearLayout viewTeamName;
    
    GridView listPlayers;
    GridView listTeams;
    
    Adapter_Player playerAdapter;
    Adapter_Player playersInTeamAdapter;
    Adapter_Team teamAdapter;
    
    int m_teamId = -1;
    boolean m_showingTeamMembers = false;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_and_teams);
        
        try
        {
            context = this;
            inflater = getLayoutInflater();
            imm = (InputMethodManager)getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
            
            InputStream in = getResources().openRawResource(R.raw.markov_weights);
            markovChain = new Util_MarkovChain(in);
            
            btnGenPlayerName = findViewById(R.id.btnGenPlayerName);
            btnAddPlayer = findViewById(R.id.btnAddPlayer);
            btnDeletePlayers = findViewById(R.id.btnDeletePlayers);
            btnAddToTeam = findViewById(R.id.btnAddToTeam);
            btnClearPlayerSelection = findViewById(R.id.btnClearPlayerSelection);
            
            btnRemoveFromTeam = findViewById(R.id.btnRemoveFromTeam);
            btnDeleteTeam = findViewById(R.id.btnDeleteTeam);
            btnShowTeamList = findViewById(R.id.btnShowTeamList);
            btnClearTeamSelection = findViewById(R.id.btnClearTeamSelection);
            btnGenTeamName = findViewById(R.id.btnGenTeamName);
            btnAddTeam = findViewById(R.id.btnAddTeam);
            
            txtPlayerName = findViewById(R.id.txtPlayerName);
            txtTeamName = findViewById(R.id.txtTeamName);
            txtTeamDescription = findViewById(R.id.txtTeamDescription);
            
            viewTeamOptions = findViewById(R.id.viewTeamOptions);
            viewTeamName = findViewById(R.id.viewTeamName);
            
            listPlayers = findViewById(R.id.gridPlayers);
            listTeams = findViewById(R.id.gridTeams);
            
            playerAdapter = new Adapter_Player(context, gAppState.getPlayers());
            teamAdapter = new Adapter_Team(context, gAppState.getTeams());
            
            listPlayers.setAdapter(playerAdapter);
            listTeams.setAdapter(teamAdapter);
            
            addListeners();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void onResume()
    {
        super.onResume();
        loadPlayersAndTeams();
    }
    
    @Override
    protected void onPause()
    {
        super.onPause();
        gSaveAppState(context);
    }
    
    @Override
    public void onBackPressed()
    {
        if(m_showingTeamMembers)
        {
            showTeamList();
        }
        else
        {
            super.onBackPressed();
        }
    }
    
    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
        if(event.getAction() == MotionEvent.ACTION_DOWN)
        {
            View v = getCurrentFocus();
            
            if(v instanceof EditText)
            {
                int rawX = (int)event.getRawX();
                int rawY = (int)event.getRawY();
                
                Rect txtRect = new Rect();
                Rect btnRndRect = new Rect();
                Rect btnAddRect = new Rect();
                
                v.getGlobalVisibleRect(txtRect);
                
                if(v == txtPlayerName)
                {
                    btnGenPlayerName.getGlobalVisibleRect(btnRndRect);
                    btnAddPlayer.getGlobalVisibleRect(btnAddRect);
                }
                else if(v == txtTeamName)
                {
                    btnGenTeamName.getGlobalVisibleRect(btnRndRect);
                    btnAddTeam.getGlobalVisibleRect(btnAddRect);
                }
                
                if(!txtRect.contains(rawX, rawY) && !btnRndRect.contains(rawX, rawY) && !btnAddRect.contains(rawX, rawY))
                {
                    v.clearFocus();
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    
                    return true;
                }
            }
        }
        
        return super.dispatchTouchEvent(event);
    }
    
    private void onPlayersChanged()
    {
        if(gAppState.isAnyPlayerSelected())
        {
            btnAddToTeam.setEnabled(m_showingTeamMembers);
            btnDeletePlayers.setEnabled(true);
            btnClearPlayerSelection.setEnabled(true);
        }
        else
        {
            btnAddToTeam.setEnabled(false);
            btnDeletePlayers.setEnabled(false);
            btnClearPlayerSelection.setEnabled(false);
        }
        
        playerAdapter.notifyDataSetChanged();
        if(m_showingTeamMembers)
        {
            playersInTeamAdapter.notifyDataSetChanged();
        }
    }
    
    private void onPlayersChanged(int teamId)
    {
        if(gAppState.isAnyPlayerSelected(teamId))
        {
            btnRemoveFromTeam.setEnabled(true);
            btnClearTeamSelection.setEnabled(true);
        }
        else
        {
            btnRemoveFromTeam.setEnabled(false);
            btnClearTeamSelection.setEnabled(false);
        }
        
        playerAdapter.notifyDataSetChanged();
        playersInTeamAdapter.notifyDataSetChanged();
    }
    
    private void showTeamList()
    {
        if(listTeams.getAdapter() != teamAdapter)
        {
            listTeams.setAdapter(teamAdapter);
        }
        
        txtTeamDescription.setText("Team List");
        viewTeamOptions.setVisibility(View.GONE);
        viewTeamName.setVisibility(View.VISIBLE);
        
        m_showingTeamMembers = false;
    }
    
    private void showTeamMembers(int teamId)
    {
        if(listTeams.getAdapter() == teamAdapter)
        {
            playersInTeamAdapter = new Adapter_Player(context, gAppState.getPlayers(teamId));
            listTeams.setAdapter(playersInTeamAdapter);
        }
        
        txtTeamDescription.setText(gAppState.getTeamName(teamId) + " Members");
        viewTeamOptions.setVisibility(View.VISIBLE);
        viewTeamName.setVisibility(View.GONE);
        
        m_showingTeamMembers = true;
        onPlayersChanged(teamId);
    }
    
    private void createPlayer()
    {
        String name = txtPlayerName.getText().toString();
        if(!name.isEmpty())
        {
            if(gAppState.createPlayer(name))
            {
                if(txtPlayerName.hasFocus())
                {
                    imm.hideSoftInputFromWindow(txtPlayerName.getWindowToken(), 0);
                    txtPlayerName.clearFocus();
                }
                
                playerAdapter.notifyDataSetChanged();
                txtPlayerName.setText(null);
                
                listPlayers.post(new Runnable()
                {
                    public void run()
                    {
                        listPlayers.smoothScrollToPosition(gAppState.getNumPlayers());
                    }
                });
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Player already exist, enter unique name", Toast.LENGTH_SHORT).show();
            }
        }
    }
    
    private void createTeam()
    {
        String name = txtTeamName.getText().toString();
        if(!name.isEmpty())
        {
            if(gAppState.createTeam(name))
            {
                if(txtTeamName.hasFocus())
                {
                    imm.hideSoftInputFromWindow(txtTeamName.getWindowToken(), 0);
                    txtTeamName.clearFocus();
                }
                
                teamAdapter.notifyDataSetChanged();
                txtTeamName.setText(null);
                
                listTeams.post(new Runnable()
                {
                    public void run()
                    {
                        listTeams.smoothScrollToPosition(gAppState.getNumPlayers());
                    }
                });
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Team already exist, enter unique name", Toast.LENGTH_SHORT).show();
            }
        }
    }
    
    private void loadPlayersAndTeams()
    {
        showTeamList();
        onPlayersChanged();
        
        txtPlayerName.setText(null);
        btnAddPlayer.setEnabled(false);
        
        txtTeamName.setText(null);
        btnAddTeam.setEnabled(false);
    }
    
    private void addListeners()
    {
        btnGenPlayerName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    txtPlayerName.setText(markovChain.generateName());
                }
                catch(Exception e)
                {
                    Log.d(TAG, e.getMessage());
                }
            }
        });
        
        btnAddPlayer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    createPlayer();
                }
                catch(Exception e)
                {
                    Log.d(TAG, e.getMessage());
                }
            }
        });
        
        btnAddToTeam.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(m_teamId > -1)
                {
                    gAppState.addSelectedPlayersToTeam(m_teamId);                    
                    onPlayersChanged();
                    
                    listTeams.post(new Runnable()
                    {
                        public void run()
                        {
                            listTeams.smoothScrollToPosition(gAppState.getNumPlayers(m_teamId));
                        }
                    });
                }
            }
        });
        
        btnDeletePlayers.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                gAppState.deleteSelectedPlayers();
                onPlayersChanged();
            }
        });
        
        btnClearPlayerSelection.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                gAppState.clearSelectedPlayers();
                onPlayersChanged();
            }
        });
        
        btnRemoveFromTeam.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(m_teamId > -1)
                {
                    gAppState.removeSelectedPlayersFromTeam(m_teamId);
                    onPlayersChanged(m_teamId);
                }
            }
        });
        
        btnDeleteTeam.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                gAppState.deleteTeam(m_teamId, true);
                playerAdapter.notifyDataSetChanged();
                showTeamList();
            }
        });
        
        btnShowTeamList.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showTeamList();
            }
        });
        
        btnClearTeamSelection.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                gAppState.clearSelectedPlayers(m_teamId);
                onPlayersChanged(m_teamId);
            }
        });
        
        btnGenTeamName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    txtTeamName.setText(markovChain.generateName());
                }
                catch(Exception e)
                {
                    Log.d(TAG, e.getMessage());
                }
            }
        });
        
        btnAddTeam.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    createTeam();
                }
                catch(Exception e)
                {
                    Log.d(TAG, e.getMessage());
                }
            }
        });
        
        txtPlayerName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence text, int start, int count, int after) {}
            
            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {}
            
            @Override
            public void afterTextChanged(Editable e)
            {
                btnAddPlayer.setEnabled(!txtPlayerName.getText().toString().isEmpty());
            }
        });
        
        txtTeamName.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence text, int start, int count, int after) {}
            
            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {}
            
            @Override
            public void afterTextChanged(Editable e)
            {
                btnAddTeam.setEnabled(!txtTeamName.getText().toString().isEmpty());
            }
        });
        
        txtPlayerName.setOnEditorActionListener(new EditText.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if(actionId == EditorInfo.IME_ACTION_DONE)
                {
                    createPlayer();
                    return true;
                }
                
                return false;
            }
        });
        
        txtTeamName.setOnEditorActionListener(new EditText.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if(actionId == EditorInfo.IME_ACTION_DONE)
                {
                    createTeam();
                    return true;
                }
                
                return false;
            }
        });
        
        listPlayers.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int pos, long id)
            {
                onPlayersChanged();
            }
        });
        
        listTeams.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int pos, long id)
            {
                if(m_showingTeamMembers)
                {
                    onPlayersChanged(m_teamId);
                }
                else
                {
                    m_teamId = pos;
                    showTeamMembers(m_teamId);
                    onPlayersChanged();
                }
            }
        });
    }
}



/*

When generating team names, we could generate one word and randomly put that in a string like:
* 
* <word> Gang/Crew/Family
* Team <word>
* 
*/
