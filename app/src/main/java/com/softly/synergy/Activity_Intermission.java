package com.softly.synergy;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.graphics.Color;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;
import java.util.Date;
import android.content.Intent;
import android.util.Log;

public class Activity_Intermission extends Activity_Base
{
    private final String TAG = "synergy";

    Button btnContinue;
    Button btnQuit;
    
    boolean started = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(TAG, "OnCreate IntermissionActivity");

        try
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_intermission);

            btnContinue = findViewById(R.id.btnContinue);
            btnQuit = findViewById(R.id.btnQuit);

            addButtonListeners();
        }
        catch(Exception e)
        {
            Log.d(TAG, "Something went wrong...");
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    protected void onResume()
    {
        Log.d(TAG, "OnResume IntermissionActivity");
    
        super.onResume();
        
        if(started)
        {
            gAppState.gameState.curRound++;
            Log.d(TAG, "round: " + gAppState.gameState.curRound);

            if(gAppState.gameState.curRound < gAppState.gameConf.roundLimit)
            {
                btnContinue.setText("Ready");
                btnQuit.setVisibility(View.VISIBLE);
            }
            else
            {
                btnContinue.setText("Finish");
                btnQuit.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            started = true;
        }
    }

    public void addButtonListeners()
    {
        // Button Next
        btnContinue.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(gAppState.gameState.curRound < gAppState.gameConf.roundLimit)
                {
                    Intent game = new Intent(Activity_Intermission.this, Activity_Game.class);
                    startActivity(game);
                }
                else
                {
                    quit();
                }
            }
        });

        // Button Quit
        btnQuit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                quit();
            }
        });
    }
    
    public void quit()
    {
        finish();
    }
}
