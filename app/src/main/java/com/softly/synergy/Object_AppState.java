package com.softly.synergy;

import java.util.ArrayList;

public class Object_AppState implements java.io.Serializable
{
    private static final int MAX_PLAYERS = 256;
    private static final int MAX_TEAMS = 256;
    
    private ArrayList<Object_Player> teamlessPlayers;
    private ArrayList<Object_Team> teams;
    private int totalPlayerAmount;

    public Object_Configuration gameConf;
    public Object_GameState gameState; // Should Object_GameState be here?
    
    public Object_AppState()
    {
        teamlessPlayers = new ArrayList<Object_Player>();
        teams = new ArrayList<Object_Team>();
        totalPlayerAmount = 0;
        
        gameConf = new Object_Configuration();
        gameState = new Object_GameState();
    }
    
    private int getTeamIndexFromString(String name) // -1 means team not found
    {
        int teamIndex = 0;
        for(; teamIndex < teams.size(); ++teamIndex)
        {
            if(name.equals(teams.get(teamIndex).name)) { break; }
        }
        
        return (teamIndex < teams.size()) ? teamIndex : -1;
    }

    
    private boolean isTeamNameUnique(String name)
    {
        return (getTeamIndexFromString(name) == -1) ? true : false;
    }
    
    private int getPlayerIndexFromString(String name, String teamName) // -1 means player not found
    {
        int playerIndex = 0;
        
        if(teamName == "")
        {
            for(; playerIndex < teamlessPlayers.size(); ++playerIndex)
            {
                if(name.equals(teamlessPlayers.get(playerIndex).name)) { break; }
            }
            
            if(playerIndex == teamlessPlayers.size()) { playerIndex = -1; }
        }
        else
        {
            int teamIndex = getTeamIndexFromString(teamName);
            
            if(teamIndex > -1)
            {
                for(; playerIndex < teams.get(teamIndex).members.size(); ++playerIndex)
                {
                    if(name.equals(teams.get(teamIndex).members.get(playerIndex).name)) { break; }
                }
                
                if(playerIndex == teams.get(teamIndex).members.size()) { playerIndex = -1; }
            }
        }
        
        return playerIndex;
    }
    
    private int getPlayerIndexFromString(String name)
    {
        return getPlayerIndexFromString(name, "");
    }
    
    private boolean isPlayerNameUnique(String name)
    {
        boolean unique = (getPlayerIndexFromString(name) == -1) ? true : false;
        
        for(int teamIndex = 0; unique && teamIndex < teams.size(); ++teamIndex)
        {
            unique = (getPlayerIndexFromString(name, teams.get(teamIndex).name) == -1) ? true : false;
        }
        
        return unique;
    }
    
    public boolean createPlayer(String name)
    {
        boolean valid = (totalPlayerAmount < MAX_PLAYERS && isPlayerNameUnique(name));
        
        if(valid)
        {
            teamlessPlayers.add(new Object_Player(name));
            totalPlayerAmount++;
        }
        
        return valid;
    }
    
    public void deletePlayer(int playerIndex, int teamIndex)
    {
        if(teamIndex == -1) // Teamless player
        {
            if(playerIndex > -1 && playerIndex < teamlessPlayers.size())
            {
                teamlessPlayers.remove(playerIndex);
                totalPlayerAmount--;
            }
        }
        else if(teamIndex > -1 && teamIndex < teams.size())
        {
            if(playerIndex > -1 && playerIndex < teams.get(teamIndex).members.size())
            {
                teams.get(teamIndex).members.remove(playerIndex);
                totalPlayerAmount--;
            }
        }
    }
    
    public void deletePlayer(int playerIndex)
    {
        deletePlayer(playerIndex, -1);
    }
    
    public void deleteSelectedPlayers(int teamId)
    {
        if(teamId == -1) // Teamless players
        {
            for(int player = 0; player < teamlessPlayers.size();)
            {
                if(teamlessPlayers.get(player).selected)
                {
                    deletePlayer(player);
                }
                else
                {
                    player++;
                }
            }
        }
        else
        {
            for(int player = 0; player < teams.get(teamId).members.size();)
            {
                if(teams.get(teamId).members.get(player).selected)
                {
                    deletePlayer(player, teamId);
                }
                else
                {
                    player++;
                }
            }
        }
    }
    
    public void deleteSelectedPlayers()
    {
        deleteSelectedPlayers(-1);
    }
    
    public boolean createTeam(String name, boolean temporary)
    {
        boolean valid = (teams.size() < MAX_TEAMS && isTeamNameUnique(name));
        
        if(valid)
        {
            teams.add(new Object_Team(name, temporary));
        }
        
        return valid;
    }
    
    public boolean createTeam(String name)
    {
        return createTeam(name, false);
    }
    
    public void deleteTeam(int teamIndex, boolean keepPlayers)
    {
        if(teamIndex > -1 && teamIndex < teams.size())
        {
            if(keepPlayers)
            {
                for(int player = 0; player < teams.get(teamIndex).members.size();)
                {
                    removePlayerFromTeam(player, teamIndex);
                }
            }
            else
            {
                totalPlayerAmount -= teams.get(teamIndex).members.size();
            }
            
            teams.remove(teamIndex);
        }
    }
    
    public void addPlayerToTeam(int playerIndex, int newTeamIndex)
    {
        if(newTeamIndex > -1 && newTeamIndex < teams.size() && playerIndex > -1 && playerIndex < teamlessPlayers.size())
        {
            Object_Player player = teamlessPlayers.get(playerIndex);
            teamlessPlayers.remove(playerIndex);
            
            player.selected = false;
            teams.get(newTeamIndex).members.add(player);
        }
    }
    
    public void addSelectedPlayersToTeam(int newTeamIndex)
    {
        for(int player = 0; player < teamlessPlayers.size();)
        {
            if(teamlessPlayers.get(player).selected)
            {
                addPlayerToTeam(player, newTeamIndex);
            }
            else
            {
                player++;
            }
        }
    }
    
    public void removePlayerFromTeam(int playerIndex, int oldTeamIndex)
    {
        if(oldTeamIndex > -1 && oldTeamIndex < teams.size() && playerIndex > -1 && playerIndex < teams.get(oldTeamIndex).members.size())
        {
            Object_Player player = teams.get(oldTeamIndex).members.get(playerIndex);
            teams.get(oldTeamIndex).members.remove(playerIndex);
            
            player.selected = false;
            teamlessPlayers.add(player);
        }
    }
    
    public void removeSelectedPlayersFromTeam(int oldTeamIndex)
    {
        for(int player = 0; player < teams.get(oldTeamIndex).members.size();)
        {
            if(teams.get(oldTeamIndex).members.get(player).selected)
            {
                removePlayerFromTeam(player, oldTeamIndex);
            }
            else
            {
                player++;
            }
        }
    }
    
    public void clearSelectedPlayers(int teamId)
    {
        if(teamId == -1)
        {
            for(int player = 0; player < teamlessPlayers.size(); ++player)
            {
                teamlessPlayers.get(player).selected = false;
            }
        }
        else
        {
            for(int player = 0; player < teams.get(teamId).members.size(); ++player)
            {
                teams.get(teamId).members.get(player).selected = false;
            }
        }
    }
    
    public void clearSelectedPlayers()
    {
        clearSelectedPlayers(-1);
    }
    
    public boolean isAnyPlayerSelected(int teamId)
    {
        if(teamId == -1) // Teamless players
        {
            for(int player = 0; player < teamlessPlayers.size(); ++player)
            {
                if(teamlessPlayers.get(player).selected) { return true; }
            }
        }
        else
        {
            for(int player = 0; player < teams.get(teamId).members.size(); ++player)
            {
                if(teams.get(teamId).members.get(player).selected) { return true; }
            }
        }
        
        return false;
    }
    
    public boolean isAnyPlayerSelected()
    {
        return isAnyPlayerSelected(-1);
    }
    
    public ArrayList<Object_Player> getPlayers(int teamId)
    {
        if(teamId == -1) // Teamless players
        {
            return teamlessPlayers;
        }
        else
        {
            return teams.get(teamId).getMembers();
        }
    }
    
    public ArrayList<Object_Player> getPlayers()
    {
        return getPlayers(-1);
    }
    
    public int getNumPlayers(int teamId)
    {
        if(teamId == -1) // Teamless players
        {
            return teamlessPlayers.size();
        }
        else
        {
            return teams.get(teamId).getMembers().size();
        }
    }
    
    public int getNumPlayers()
    {
        return getNumPlayers(-1);
    }
    
    public String getPlayerName(int playerIndex, int teamIndex) // -1 for teamless players
    {
        String name = "";
        
        if(teamIndex == -1)
        {
            if(playerIndex < teamlessPlayers.size())
            {
                name = teamlessPlayers.get(playerIndex).name;
            }
        }
        else if(teamIndex < teams.size())
        {
            if(playerIndex < teams.get(teamIndex).members.size())
            {
                name = teams.get(teamIndex).members.get(playerIndex).name;
            }
        }
        
        return name;
    }
    
    public String getPlayerName(int playerIndex)
    {
        return getPlayerName(playerIndex, -1);
    }
    
    public ArrayList<Object_Team> getTeams()
    {
        return teams;
    }
    
    public int getNumTeams()
    {
        return teams.size();
    }
    
    public String getTeamName(int teamIndex)
    {
        String name = "";
        
        if(teamIndex > -1 && teamIndex < teams.size())
        {
            name = teams.get(teamIndex).name;
        }
        
        return name;
    }
}
